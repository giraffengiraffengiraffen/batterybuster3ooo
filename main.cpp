/*
    Battery Buster 3ooo
    All your crappy no-name batteries are belong to us
    Copyright (C) 2015 Johannes Kroll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Platform/Platform.h>
#include "promicropinmap.h"
#include "main.h"


#define CLAMP(V, min_, max_) min(max(min_, V), max_)  //do { if(V<min) V= min; if(V>max) V= max; } while(0);
#define min(a,b) ((a)<(b)? (a): (b))
#define max(a,b) ((a)>(b)? (a): (b))

#define RESET_PINREG    PINF
#define RESET_DDR       DDRF
#define RESET_PORT      PORTF
#define RESET_PIN       4


struct settings
{
    uint16_t current;           // target current (milliamps)
    uint16_t stopvoltage;       // stop measuring when battery reaches this voltage (millivolts)
} __attribute__((packed));

settings cursettings= { 0 };

struct discharge_measurement
{
    uint32_t count;
    uint32_t milliamps_sum;
    uint32_t centivolts_sum;
    uint16_t reportinterval;    /* how often to print csv data: reportinterval * ~5 msec */
    struct
    {
        uint32_t lastmillis;
        uint32_t milliamps_sum;
        uint32_t centivolts_sum;
        uint16_t count;
        void reset() { lastmillis= milliamps_sum= centivolts_sum= count= 0; }
    } rep;
    void reset() { count= milliamps_sum= centivolts_sum= 0; reportinterval= 4*1000/5; rep.reset(); }
};

discharge_measurement curmeasurement;

enum 
{
    STATE_IDLE,
    STATE_DISCHARGING,
    STATE_DISCHARGE_FINISHED,
    STATE_ERROR,
};

uint8_t currentstate= STATE_IDLE;

/*
struct preset
{
    const char *name;
    struct settings settings;
};
preset presets[]= { ... };
// XXX or do this in some python script
*/


//////////////////////////////// ADC //////////////////////////////// 

ISR(ADC_vect)
{
    // do nothing
}

void adcSetup()
{
    ADMUX= ((1<<REFS0)|(1<<REFS1))  // internal 2.56V reference
        | 0b000111;                 // adc7/pf7/A0
    ADCSRB= 0;
}

void adcSelectVoltagePin()          // select A0 to measure voltage
{
    ADMUX= ((1<<REFS0)|(1<<REFS1))  // internal 2.56V reference
        | 0b000111;                 // adc7/pf7/A0
}

void adcSelectCurrentPin()          // select A1 to measure current using 0.1R shunt resistor
{
    ADMUX= ((1<<REFS0)|(1<<REFS1))  // internal 2.56V reference
        | 0b000110;                 // adc6/pf6/A1
}

uint16_t adcReadOnce()
{
#ifdef USE_ADC_NOISE_REDUCTION
    // ADC noise reduction/sleep mode turns off PWM while measuring, which skews measurements because the filter cap discharges, so it shouldn't be used
    SMCR= (1<<SM0) | (1<<SE);   // enable adc noise reduction
    ADCSRA= (1<<ADEN) | (1<<ADIE) | (1<<ADPS0)|(1<<ADPS1)|(1<<ADPS2)/*clock div 128 (note to self: atmega requires adc clock between 50 and 200khz, except ADHSM(?)*/;
    sleep_mode();               // trigger adc conversion
#else
    SMCR= (1<<SE);
    ADCSRA= (1<<ADEN) | (1<<ADIE) | (1<<ADSC) | (1<<ADPS0)|(1<<ADPS1)|(1<<ADPS2);
    while(ADCSRA & (1<<ADSC))
        ;
#endif
    return ADC;
}

uint32_t adcReadMulti(uint8_t n)
{
    uint32_t result= 0;
    for(uint8_t i= 0; i<n; ++i)
    {
        result+= adcReadOnce();
    }
    return result;
}

static void __attribute__((unused)) adcTest()
{
    static struct {
        uint8_t muxbits;
        const char *description;
    } adcmuxtable[]=
    {
        // pot 0
        0b000111,  "adc7/pf7/A0",
        0b000110,  "adc6/pf6/A1",
        0b000101,  "adc5/pf5/A2",
        0b000100,  "adc4/pf4/A3",
        0b100000,  "adc8/pd4/D4",
        0b100010,  "adc10/D6",
        0b100011,  "adc11/pb4/D8",
        0b100100,  "adc12/pb5/D9",
        0b100101,  "adc13/pb6/D10",
    };
    for(unsigned i= 0; i<sizeof(adcmuxtable)/sizeof(adcmuxtable[0]); ++i)
    {
        ADMUX= ((1<<REFS0)|(1<<REFS1)) | (adcmuxtable[i].muxbits&0b11111);
        ADCSRB&= ~(1<<MUX5);
        ADCSRB|= (adcmuxtable[i].muxbits>>5)<<MUX5;
        uint16_t adc= adcReadOnce();
        printf("%s: %d\r\n", adcmuxtable[i].description, adc);
    }
    puts("");
}


//////////////////////////////// ADC Range //////////////////////////////// 

static const uint32_t vdiv_r1= 22000;
static const uint8_t ddrb_mask= (1<<6)/*D10*/ | (1<<2)/*D16*/ | (1<<3)/*D14*/;
static int16_t current_range= 0;
struct rangedef
{
    uint32_t vdiv_r2;
    int16_t uptreshold;
    int16_t downtreshold;
    uint8_t ddrb_bits;
    const char *description;
};

/* 
Calculated with resistorcombinations.py:
Best voltage divider selections for R1=22000Ohm: 
	  5.1V: Range= 5.12V (100.39%), R2=22000Ohm
	   17V: Range=17.00V (100.01%), R2= 3900Ohm
	   28V: Range=28.16V (100.57%), R2= 2200Ohm
Total deviation from wanted ranges: 0.97%
*/
rangedef rangedefs[]= {
    { .vdiv_r2= 0,     .uptreshold= 1024*3/4,/*  1.92V */ .downtreshold= -1,                  .ddrb_bits= 0,    .description= "2.56V" },
    { .vdiv_r2= 22000, .uptreshold= 1024*7/8,/*  4.48V */ .downtreshold= 1024*1/4,/* 1.28V */ .ddrb_bits= 1<<6, .description= "5.12V" },    // Pin B6/D10
    { .vdiv_r2= 3900,  .uptreshold= 1024*3/4,/* 14.87V */ .downtreshold= 1024*1/4,/* 4.25V */ .ddrb_bits= 1<<2, .description= "17.00V" },   // Pin B2/D16
    { .vdiv_r2= 2200,  .uptreshold= -1,                   .downtreshold= 1024*1/4,/* 7.04V */ .ddrb_bits= 1<<3, .description= "28.16V" },   // Pin B3/D14
};

void rangeSelect(int range)
{
    printf("# rangeSelect %s\r\n", rangedefs[range].description);
    DDRB= (DDRB & (~ddrb_mask)) | rangedefs[range].ddrb_bits;
    current_range= range;
}

void autoRange(int16_t adc)
{
    if(rangedefs[current_range].uptreshold>=0 && adc > rangedefs[current_range].uptreshold)
        rangeSelect(++current_range);
    else if(rangedefs[current_range].downtreshold>=0 && adc < rangedefs[current_range].downtreshold)
        rangeSelect(--current_range);
}


//////////////////////////////// timer //////////////////////////////// 

volatile uint32_t timerOvfCount;

ISR(TIMER1_COMPA_vect)
{
    timerOvfCount++;
}

#define MILLITIMER_TOP  32000
void TimerSetup(void)
{
    // setup timer1 for clkIO/256, CTC mode, enable interrupt on output compare match
    TCCR1A= 0;
    TCCR1B= (1<<CS12) | (1<<WGM12);
    TIMSK1= (1<<OCIE1A);
    OCR1A= MILLITIMER_TOP;   // fires every (1/512) seconds
}

void TimerReset(void)
{
    uint8_t oldSREG= SREG;
    uint8_t oldTCCR1B= TCCR1B;
    cli();
    TCCR1B= 0;
    timerOvfCount= 0;
    SREG= oldSREG;
    TCCR1B= oldTCCR1B;
}

uint32_t GetMillis(void)
{
    uint8_t oldSREG= SREG;
    cli();
    uint32_t submillis= uint32_t(TCNT1) * 512 / MILLITIMER_TOP;
    uint32_t millis= timerOvfCount*512;
    SREG= oldSREG;
    return millis+submillis;
}


void statusLED(bool on)
{
    if(on)
        PORTB&= ~(1<<0);
    else
        PORTB|= 1<<0;
}

void initVoltageRange()
{
    static const int nsamples= 32;
    adcSelectVoltagePin();
    for(uint8_t i= 0; i<=sizeof(rangedefs)/sizeof(rangedefs[0]); ++i)
    {
        uint32_t adc= adcReadMulti(nsamples);
        autoRange(adc/nsamples);
    }
}


#define V_ERRORCOMP 247 // ~ /1.03
uint16_t measure_centivolts(int nsamples= 32)
{
    adcSelectVoltagePin();
    uint32_t adc= adcReadMulti(nsamples) * V_ERRORCOMP >> 8;
    uint16_t ret;
    if(rangedefs[current_range].vdiv_r2)
        ret= adc * (vdiv_r1 + rangedefs[current_range].vdiv_r2) / rangedefs[current_range].vdiv_r2 / (nsamples*4);
    else 
        ret= adc * (256/nsamples)/1024;
    autoRange(adc/nsamples);
    return ret;
}

#define SHUNT_MILLIOHM  85  // XXX todo: some kind of runtime calibration feature would be nice; save this to eeprom
uint16_t measure_milliamps(int nsamples= 32)
{
    adcSelectCurrentPin();
    uint32_t adc= adcReadMulti(nsamples);
    //~ return adc * SHUNT_MILLIOHM * (256/nsamples)/1024;  // XXX todo: division rounding error? possible reason for high error at low current levels?
    return (adc * SHUNT_MILLIOHM * (256/nsamples) + 512) >> 10;  // XXX better??
}

void measure(uint16_t &centivolts, uint16_t &milliamps)
{
    static const int nsamples= 4;

    centivolts= measure_centivolts(nsamples);
    milliamps= measure_milliamps(nsamples);
}

void showpower()
{
    uint16_t centivolts, milliamps;
    measure(centivolts, milliamps);
    
    printf("# %u.%02uV @ %umA = %umW\r\n", centivolts/100, centivolts%100, milliamps, centivolts*milliamps/100);
}

void ledflash(uint16_t on, uint16_t off)
{
    PORTD&= ~(1<<5);
    Pin<D9>::set();
    for(uint16_t i= 0; i<on/100; ++i)
        _delay_us(90);
    PORTD|= (1<<5);
    Pin<D9>::clear();
    for(uint16_t i= 0; i<off/100; ++i)
        _delay_us(90);
}

typedef void (*statetickfn)();

statetickfn stateticks[]=
{
    []() {  // IDLE
        Delay_MS(10);
    },
    []() {  // DISCHARGING
        uint16_t centivolts, milliamps;
        measure(centivolts, milliamps);
        adjustCurrent(milliamps, cursettings.current);
        curmeasurement.centivolts_sum+= centivolts;
        curmeasurement.milliamps_sum+= milliamps;
        ++curmeasurement.count;

        curmeasurement.rep.centivolts_sum+= centivolts;
        curmeasurement.rep.milliamps_sum+= milliamps;
        if(++curmeasurement.rep.count==curmeasurement.reportinterval)
        {
            uint32_t millis= GetMillis();
            uint32_t ma= (curmeasurement.rep.milliamps_sum + curmeasurement.rep.count/2) / curmeasurement.rep.count;
            uint32_t mAh= ma * (millis/1000) / (60*60); // XXX this is wrong...
            uint32_t mv= (curmeasurement.rep.centivolts_sum + curmeasurement.rep.count/20) / (curmeasurement.rep.count/10);
            printf("%lu.%03lu, %lu, %lu, %lu\r\n", millis/1000,millis%1000, mv, ma, mAh);
            curmeasurement.rep.reset();
            if(mv < cursettings.stopvoltage)
                setstate(STATE_DISCHARGE_FINISHED);
        };
        
        //~ Delay_MS(5);
        static uint16_t n;
        n= (n-20)&4095;
        ledflash(n, 5000-n);
    },
    []() {  // DISCHARGE_FINISHED
        static uint8_t ncalls;
        if(++ncalls&4)
            statusLED(1);
        else
            statusLED(0);
        if(ncalls>=8*16)
        {
            ncalls= 0;
            setstate(STATE_IDLE);
        }
        Delay_MS(125);
    },
    []() {  // ERROR
        static uint16_t ncalls;
        ++ncalls;
        bool on= (((ncalls>>3&1) ^ (ncalls>>2&1)) & (ncalls&1)) ^ (ncalls>>2&1);  // nervous blink
        statusLED(on);
        if(ncalls>=20*16)
        {
            ncalls= 0;
            setstate(STATE_IDLE);
        }
        Delay_MS(50);
    },
};


void setstate(uint8_t newstate)
{
    if(newstate==STATE_DISCHARGING)
    {
        statusLED(true);
        initVoltageRange();
        curmeasurement.reset();
        setCurrentPWM(0);
        Delay_MS(250);
        if(measure_centivolts()<2)
        {
            printf("# ERROR! low voltage. no battery connected or polarity reversed?\r\n");
            setstate(STATE_ERROR);
            return;
        }
        printf("seconds, mV, mA, mAh\r\n");
        TimerReset();
    }
    else if(currentstate==STATE_DISCHARGING && newstate!=currentstate)
    {
        if(newstate==STATE_DISCHARGE_FINISHED)
        {
            setCurrentPWM(0);
            uint32_t millis= GetMillis();
            uint32_t mAh= (curmeasurement.milliamps_sum+curmeasurement.count/2) / curmeasurement.count * (millis/1000) / (60*60);
            printf("# discharge done! result: %lusec, %lu measurements, %lumAh, %lumWh\r\n",  // XXX todo: check if mwh calculation is still wrong
                GetMillis()/1000, curmeasurement.count, mAh, mAh*(curmeasurement.centivolts_sum+curmeasurement.count/2)/curmeasurement.count);
        }
    }
    currentstate= newstate;
}


#define PWM_MAX 1023

// configure Timer0 as PWM output for the constant current load
void setupCurrentTimer()
{
    TCCR3A= (1<<COM3A1) |   // Clear OC3A on compare match, set OC3A at TOP
            (1<<WGM30) |    // Fast PWM, 10 bit
            (1<<WGM31);     // Fast PWM, 10 bit
    TCCR3B= (1<<WGM32) |    // Fast PWM, 10 bit
            (1<<CS30);      // ClkI/O / 1 (No prescaling)
    DDRC|= (1<<6);          // Enable output pin C6 aka D5
    OCR3A= 0;               // Set inital PWM value
}

uint16_t getCurrentPWM()
{
    return OCR3A;
}
void setCurrentPWM(uint16_t v)
{
    cli();
    OCR3A= v;
    sei();
}
void adjustCurrent(uint16_t measured_mA, uint16_t target_mA)
{
    if(target_mA==0)
        OCR3A= 0;
    else if(measured_mA<target_mA)
    {
        if(OCR3A<PWM_MAX)
            ++OCR3A;
    }
    else if(measured_mA>target_mA)
    {
        if(OCR3A>0)
            --OCR3A;
    }
}

template<PromicroPin P>
void testpin()
{
    if(P!=A3)
    {
        printf("testing pin %d\r\n", P);
        Pin<P>::asOutput();
        Pin<P>::set();
        Delay_MS(250);
        Pin<P>::clear();
    }
    if(P) testpin<PromicroPin(unsigned(P-1)%D17)>();
}


// app setup
void setup(void)
{    
    adcSetup();
    rangeSelect(0);
    
    RESET_PINREG|= (1<<RESET_PIN);
    RESET_DDR|= (1<<RESET_PIN);
    
    // status LED
    DDRB|= (1<<0);
    statusLED(false);
    
    TimerSetup();
    setupCurrentTimer();
    
    DDRD|= (1<<5);  // switch green led port to output mode
    PORTD|= (1<<5); // turn green led off
    Pin<D9>::asOutput();    // enable extra led port & turn it off
    Pin<D9>::clear();
}


#ifdef DEBUGTICK
uint16_t target_mA= 0;
#endif 


// called periodically from main loop
void tick(void)
{
#ifdef DEBUGTICK
    uint16_t cv, ma;
    measure(cv, ma);
    adjustCurrent(ma, target_mA);

    enum { navg= 256 };
    static uint16_t last_mA[navg];
    static uint8_t mai;

    last_mA[++mai&(navg-1)]= ma;
    uint32_t sum= 0;
    for(unsigned i= 0; i<navg; ++i)
        sum+= last_mA[i];
    if(!(mai&(navg-1)))
        printf("OCR3A: %04d - avg mA: %03lu\r\n", OCR3A, (sum+navg/2)/navg);

    Delay_MS(5);
#else    
    if(stateticks[currentstate])
        stateticks[currentstate]();
#endif
}



//////////////////////////////// Serial Commands //////////////////////////////// 

struct command
{
    const char *names[2];
    const char *helptext;
    void (*func)(const char *args);
};

void cmd_help(const char *args);

static command commands[]= 
{
    {
        { "reset", "R" },
        "reset uC (jump to bootloader for programming)",
        [] (const char *args) {
            RESET_PORT&= ~(1<<RESET_PIN);
        }
    },
    {
        { "current", "c" },
        "get or set constant current to draw (milliamps)",
        [] (const char *args) {
            int16_t current;
            if(sscanf(args, "%d", &current)==1)
                cursettings.current= current;
            printf("# discharge current: %u\r\n", cursettings.current);
        }
    },
    {
        { "cutoff", "C" },
        "get or set cut-off voltage (millivolts)",
        [] (const char *args) {
            uint32_t v= 0;
            if(sscanf(args, "%lu", &v)==1)
                cursettings.stopvoltage= CLAMP(v, 0, 28000);
            printf("# cut-off voltage: %umV\r\n", cursettings.stopvoltage);
        }
    },
    {
        { "discharge", "d" },
        "run discharge test.",
        [] (const char *args) {
            if(cursettings.current==0)
            {
                puts("# set current first");
                return;
            }
            if(cursettings.stopvoltage==0)
            {
                puts("# set cut-off voltage first");
                return;
            }
            printf("# discharging down to %umV with constant current %d mA...\r\n", cursettings.stopvoltage, cursettings.current);
            setstate(STATE_DISCHARGING);
        }
    },
    {
        { "help", "h" },
        "online help",
        cmd_help
    },
#define DEBUG
#ifdef DEBUG
#ifdef DEBUGTICK
    {
        { "ma", "m" },
        "milliamps debug stuff",
        [] (const char *args) {
            uint16_t t;
            if(sscanf(args, "%u", &t)==1)
                target_mA= t;
            printf("target_mA: %d\r\n", target_mA);
        }
    },
#endif //DEBUGTICK
    {
        { "measurement-test", "mt" },
        "voltage and current measurement debug stuff",
        [] (const char *args) {
            uint16_t prevCentivolts= 0xFFFF, prevMilliamps= 0xFFFF;
            for(int i= 0; i<1000 && prevCentivolts!=0; ++i)
            {
                uint16_t centivolts, milliamps;
                measure(centivolts, milliamps);
                if((centivolts != prevCentivolts) || (prevMilliamps != milliamps))
                {
                    printf("%2d.%02dV, %d.%03dA\r\n", centivolts/100,centivolts%100, milliamps/1000,milliamps%1000);
                    prevCentivolts= centivolts;
                    prevMilliamps= milliamps;
                }
                extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
                CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
                USB_USBTask();
                _delay_ms(100);
            }
        }
    },
#endif // DEBUG
};

void cmd_help(const char *args)
{
    printf("# * command help:\r\n");
    for(uint8_t i= 0; i<sizeof(commands)/sizeof(commands[0]); ++i)
    {
        printf("# %s", commands[i].names[0]);
        if(commands[i].names[1]) printf(", %s", commands[i].names[1]);
        printf("\t%s\r\n", commands[i].helptext);
    }
}


bool ProcessCDCLine(const char *line)
{
    for(uint8_t i= 0; i<sizeof(commands)/sizeof(commands[0]); ++i)
    {
        for(uint8_t k= 0; k<2; ++k)
        {
            uint8_t cmdlen= strlen(commands[i].names[k]);
            if( (strncmp(line, commands[i].names[k], cmdlen)==0) && (line[cmdlen]==' '||line[cmdlen]==0) )
            {
                const char *args= line+cmdlen;
                if(*args) ++args;
                commands[i].func(args);
                return true;
            }
        }
    }
    
    return false;
}

void ProcessCDCChar(uint8_t c)
{
    #define LINE_MAX 32
    static char linebuffer[LINE_MAX+1];
    static int offset= 0;
    
    if(offset>LINE_MAX)
        offset= LINE_MAX;
    
    if(c=='\n' || c=='\r')
    {
        linebuffer[offset]= 0;
        while(offset>0 && isspace(linebuffer[offset]))
            linebuffer[--offset]= 0;
        //~ printf("'%s'\n", linebuffer);
        if(strlen(linebuffer))
        {
            if(!ProcessCDCLine(linebuffer))
                cmd_help("");
        }
        offset= 0;
    }
    else
        linebuffer[(offset++)&(LINE_MAX-1)]= c;
}

