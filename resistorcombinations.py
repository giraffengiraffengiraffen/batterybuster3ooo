#!/usr/bin/python
# -*- coding:utf-8 -*-
# Battery Buster 3ooo
# All your crappy no-name batteries are belong to us
# Copyright (C) 2015 Johannes Kroll

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def get_range(adc_ref, R1, R2):
    return float(adc_ref) * (R1+R2) / R2

if __name__ == '__main__':
    R1_available= sorted([
        10000,
        47000,
        56000,
        33000,
        22000,
        39000,
    ])

    R2_available= sorted([
        2200,
        1500,
        3900,
        1000,
        3300,
        10000,
        5600,
        4700,
        47000,
        75000,
        56000,
        33000,
        68000,
        39000,
        82000,
        22000,
        15000,
        7500,
        6800,
        8200
    ])
    
    adc_ref= 2.56
    
    wanted_ranges= [ 5.1, 17, 28 ]
    
    best_ranges= {}
    
    print "ADC Reference is %.2fV.\n" % adc_ref
    
    # get value of R2 for each R1 which yields the closest value above each wanted_ranges entry.
    for R1 in R1_available:
        best_ranges[R1]= [ 
            { "R2": 0,
              "wanted_range": rng,
              "actual_range": 1000000
            } for rng in wanted_ranges 
        ]
        
        for R2 in R2_available:
            current_range= get_range(adc_ref, R1, R2)
            for i in range( len(wanted_ranges) ):
                if current_range >= best_ranges[R1][i]["wanted_range"] \
                    and current_range < best_ranges[R1][i]["actual_range"]:
                    best_ranges[R1][i]["R2"]= R2
                    best_ranges[R1][i]["actual_range"]= current_range
    
    # calculate total deviation for each R1.
    r= []
    for R1 in best_ranges:
        total_dev= 0
        for best in best_ranges[R1]:
            best["deviation"]= best["actual_range"]*100/best["wanted_range"]
            total_dev+= best["deviation"] - 100
        r.append( { "total_dev": total_dev, "R1": R1, "sel": best_ranges[R1] } )
    
    # print sorted result.
    for crng in sorted(r, lambda a, b: -1 if a["total_dev"]<b["total_dev"] else 0 if a["total_dev"]==b["total_dev"] else 1):
        print "Best voltage divider selections for R1=%sΩ: " % crng["R1"]
        for best in crng["sel"]:
            print "\t%(wanted_range)5sV: Range=%(actual_range)5.2fV (%(deviation)5.2f%%), R2=%(R2)5sΩ" % best
        print "Total deviation from wanted ranges: %.2f%%" % crng["total_dev"]
        print ""
    