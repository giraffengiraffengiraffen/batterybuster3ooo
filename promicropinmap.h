/*
    Battery Buster 3ooo
    All your crappy no-name batteries are belong to us
    Copyright (C) 2015 Johannes Kroll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROMICROPINMAP_H
#define PROMICROPINMAP_H
#ifndef _AVR_IO_H_
#include <avr/iom32u4.h>
#endif

enum PromicroPin
{
    A0, PF7_=A0,
    A1, PF6_=A1,
    A2, PF5_=A2,
    A3, PF4_=A3,
    A4, PF1_=A4,
    A5, PF0_=A5,
    D0, PD2_=D0,
    D1, PD3_=D1,
    D2, PD1_=D2,
    D3, PD0_=D3,
    D4, A6=D4, PD4_=D4,
    D5, PC6_=D5,
    D6, A7=D6, PD7_=D6,
    D7, PE6_=D7,
    D8, PB4_=D8,
    D9, A8=D9, PB5_=D9,
    D10, PB6_=D10,
    D11, PB7_=D11,
    D12, A10_=D12, PD6_=D12,
    D13, PC7_=D13,
    D14, PB3_=D14,
    D15, PB1_=D15,
    D16, PB2_=D16,
    D17, PB0_=D17
};

enum PromicroPort
{
    PORT_A, PORT_B, PORT_C, PORT_D, PORT_E, PORT_F 
};

template<PromicroPort P>
struct Port
{
};

#define MKPORTDEF(P) template<> struct Port< PORT_##P > { enum { PORTREG= _SFR_MEM_ADDR(PORT##P), DDRREG= _SFR_MEM_ADDR(DDR##P), PINREG= _SFR_MEM_ADDR(PIN##P) }; };
//~ MKPORTDEF(A);
MKPORTDEF(B);
MKPORTDEF(C);
MKPORTDEF(D);
MKPORTDEF(E);
MKPORTDEF(F);

template<PromicroPort PORTID, uint8_t PINBIT>
struct PinMap
{
    static void set() { _MMIO_BYTE(Port<PORTID>::PORTREG) |= (1<<PINBIT); }
    static void clear() { _MMIO_BYTE(Port<PORTID>::PORTREG) &= ~(1<<PINBIT); }
    static void output(bool on) { if(on) set(); else clear(); }
    static bool output() { return (_MMIO_BYTE(Port<PORTID>::PORTREG) & (1<<PINBIT)); }
    static bool input() { return (_MMIO_BYTE(Port<PORTID>::PINREG) & (1<<PINBIT)); }
    static void asOutput() { _MMIO_BYTE(Port<PORTID>::DDRREG) |= (1<<PINBIT); }
    static void asInput() { _MMIO_BYTE(Port<PORTID>::DDRREG) &= ~(1<<PINBIT); }
};

template<PromicroPin P>
struct Pin
{};

template<> struct Pin<A0>:  public PinMap< PORT_F, 7 > {};
template<> struct Pin<A1>:  public PinMap< PORT_F, 6 > {};
template<> struct Pin<A2>:  public PinMap< PORT_F, 5 > {};
template<> struct Pin<A3>:  public PinMap< PORT_F, 4 > {};
template<> struct Pin<A4>:  public PinMap< PORT_F, 1 > {};
template<> struct Pin<A5>:  public PinMap< PORT_F, 0 > {};
template<> struct Pin<D0>:  public PinMap< PORT_D, 2 > {};
template<> struct Pin<D1>:  public PinMap< PORT_D, 3 > {};
template<> struct Pin<D2>:  public PinMap< PORT_D, 1 > {};
template<> struct Pin<D3>:  public PinMap< PORT_D, 0 > {};
template<> struct Pin<D4>:  public PinMap< PORT_D, 4 > {};
template<> struct Pin<D5>:  public PinMap< PORT_C, 6 > {};
template<> struct Pin<D6>:  public PinMap< PORT_D, 7 > {};
template<> struct Pin<D7>:  public PinMap< PORT_E, 6 > {};
template<> struct Pin<D8>:  public PinMap< PORT_B, 4 > {};
template<> struct Pin<D9>:  public PinMap< PORT_B, 5 > {};
template<> struct Pin<D10>: public PinMap< PORT_B, 6 > {};
template<> struct Pin<D11>: public PinMap< PORT_B, 7 > {};
template<> struct Pin<D12>: public PinMap< PORT_D, 6 > {};
template<> struct Pin<D13>: public PinMap< PORT_C, 7 > {};
template<> struct Pin<D14>: public PinMap< PORT_B, 3 > {};
template<> struct Pin<D15>: public PinMap< PORT_B, 1 > {};
template<> struct Pin<D16>: public PinMap< PORT_B, 2 > {};
template<> struct Pin<D17>: public PinMap< PORT_B, 0 > {};

#endif //PROMICROPINMAP_H
