#
#             LUFA Library
#     Copyright (C) Dean Camera, 2014.
#
#  dean [at] fourwalledcubicle [dot] com
#           www.lufa-lib.org
#
# --------------------------------------
#         LUFA Project Makefile.
# --------------------------------------

# Run "make help" for target help.

MCU          = atmega32u4
ARCH         = AVR8
BOARD        = USER
F_CPU        = 16000000
F_USB        = $(F_CPU)
OPTIMIZATION = s
TARGET       = main
SRC          = $(wildcard *.c) $(wildcard *.cpp) $(wildcard lufa/*.c) $(LUFA_SRC_USB) $(LUFA_SRC_USBCLASS)
LUFA_PATH    = $(HOME)/src/lufa-LUFA-140302/LUFA
CC_FLAGS     = -DUSE_LUFA_CONFIG_HEADER -IConfig/ -I. -Ilufa -std=c++11
LD_FLAGS     =

# Default target
all:

# Include LUFA build script makefiles
include $(LUFA_PATH)/Build/lufa_core.mk
include $(LUFA_PATH)/Build/lufa_sources.mk
include $(LUFA_PATH)/Build/lufa_build.mk
include $(LUFA_PATH)/Build/lufa_cppcheck.mk
include $(LUFA_PATH)/Build/lufa_doxygen.mk
include $(LUFA_PATH)/Build/lufa_dfu.mk
include $(LUFA_PATH)/Build/lufa_hid.mk
include $(LUFA_PATH)/Build/lufa_avrdude.mk
include $(LUFA_PATH)/Build/lufa_atprogram.mk

upload: all
	stty -F /dev/ttyACM0 115200
	bash -c "(echo; echo R) > /dev/ttyACM?"
	#~ bash -c "(echo; echo reset) > /dev/ttyACM?"
	#~ echo R | cu -t -l /dev/ttyACM0
	until lsusb|grep -i "ID 239a"; do sleep 0.1; done
	#~ while ! stat $(SERIAL) >/dev/null ; do sleep .5; done
	-avrdude -p m32u4 -c avr109 -P $(shell echo /dev/ttyACM?) -U flash:w:$(TARGET).hex &
	until lsusb|grep -i "ID 03eb:2044"; do sleep 0.1; done
	-sleep .1; gtkterm -p $(shell echo /dev/ttyACM?) -s 115200

