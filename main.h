/*
    Battery Buster 3ooo
    All your crappy no-name batteries are belong to us
    Copyright (C) 2015 Johannes Kroll

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOUCHPADTEST_H
#define TOUCHPADTEST_H

#ifdef __cplusplus
extern "C" {
#endif
void setup(void);
void tick(void);
void ProcessCDCChar(uint8_t c);
void TimerReset(void);
uint32_t GetMillis(void);
uint8_t getFets(void);
void statusLED(bool on);
void setstate(uint8_t newstate);
void initVoltageRange(void);
uint16_t getCurrentPWM();
void setCurrentPWM(uint16_t v);
void adjustCurrent(uint16_t measured_mA, uint16_t target_mA);
#ifdef __cplusplus
};
#endif

#endif //TOUCHPADTEST_H
